set -e
echo "Building hextroller"
mkdir -p modules/hextroller
go build -o modules/hextroller/hextroller .
echo "Copying genesis"
rsync -av ../CommandControl/genesis/genesis .
