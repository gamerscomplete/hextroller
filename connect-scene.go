package main

import (
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gui"
	"sync"
)

type ConnectScene struct {
	*core.Node
	serverAddr    string
	serverPort    int
	statusMessage string
	connecting    bool
	connLock      sync.Mutex
}

func NewConnectScene(connFun func(string, int)) (*ConnectScene, error) {
	// Image button with text and multiple states
	connectScene := ConnectScene{Node: core.NewNode()}
	connectButton, err := gui.NewImageButton("images/blue_normal.png")
	if err != nil {
		return nil, err
	}
	connectButton.SetPosition(10, 10)
	connectButton.SetText("Connect")
	//  connectButton.SetSize(40, 40)
	connectButton.SetFontSize(20)
	err = connectButton.SetImage(gui.ButtonOver, "images/blue_over.png")
	if err != nil {
		return nil, err
	}
	err = connectButton.SetImage(gui.ButtonPressed, "images/blue_pressed.png")
	if err != nil {
		return nil, err
	}
	connectButton.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		go connFun(SERVER_ADDR, SERVER_PORT)
	})
	connectScene.Add(connectButton)

	return &connectScene, nil
}

func (connectScene *ConnectScene) Start() {

}

func (connectScene *ConnectScene) Stop() {

}
