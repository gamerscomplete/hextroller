package main

import (
	"fmt"
	"gitlab.com/g3n/engine/app"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gui"
	//	"gitlab.com/g3n/engine/math32"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/hexapod/client"
	"gitlab.com/gamerscomplete/uuid"
	//	"strconv"
	//	"time"
)

type Hextroller struct {
	serverConn *csTools.ServerConnObj
	botClient  *botclient.Client

	rootScene    *core.Node
	runScene     *RunScene
	connectScene *ConnectScene
	currentScene Scene
	peer         uuid.UUID

	app *app.Application
}

func NewHextroller() *Hextroller {
	return &Hextroller{}
}

func (hextroller *Hextroller) switchScene(scene Scene) {
	hextroller.rootScene.Remove(hextroller.currentScene)
	hextroller.rootScene.Add(scene)
	if hextroller.currentScene != nil {
		hextroller.currentScene.Stop()
	}
	hextroller.currentScene = scene
	gui.Manager().Set(hextroller.currentScene)
	scene.Start()
}

func (hextroller *Hextroller) setupServerConn() error {
	hextroller.serverConn = csTools.NewServerConn()
	//    hexapod.serverConn.RegisterHandler("GET_MODE", hexapod.getMode)
	if err := hextroller.serverConn.Init("hextroller"); err != nil {
		return err
	}
	return nil
}

func (hextroller *Hextroller) connectToServer(addr string, port int) {
	if hextroller.connectScene.connecting {
		return
	}

	hextroller.connectScene.connLock.Lock()
	defer hextroller.connectScene.connLock.Unlock()
	hextroller.connectScene.connecting = true
	peerID, err := hextroller.serverConn.ConnectTo(addr, port)
	if err != nil {
		hextroller.connectScene.connecting = false
		fmt.Println("Failed to connect to server:", err)
		return
	}
	hextroller.runScene.botClient = botclient.NewClient(hextroller.serverConn, peerID)
	hextroller.peer = peerID
	hextroller.runScene.peer = peerID
	hextroller.connectScene.connecting = false
	fmt.Println("Connection successful to: ", peerID.String())
	hextroller.switchScene(hextroller.runScene)
}

func (hextroller *Hextroller) Connected() bool {
	if hextroller.runScene.botClient != nil {
		return true
	}
	return false
}
