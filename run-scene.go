package main

import (
	"fmt"
	"gitlab.com/g3n/engine/app"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/graphic"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/light"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/texture"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/hexapod/client"
	"gitlab.com/gamerscomplete/uuid"
	"strconv"
	"time"
)

type RunScene struct {
	*core.Node
	monitor     monitor
	warnLatency int
	serverConn  *csTools.ServerConnObj
	botClient   *botclient.Client

	savedPoses  map[string][][]float32
	currentPose [][]float32

	peer        uuid.UUID
	spiderModel SpiderModel

	servoConfiguration [][]uint8
}

type monitor struct {
	stopMonitor        chan bool
	latencyLabel       *gui.Label
	latencyText        string
	servoConfiguration [][]uint8

	hextrollerBatLabel *gui.Label
	hextrollerBatText  string

	//100%=upper voltage 0%=lower voltage
	hexapodBatLabel *gui.Label
	hexapodBatText  string

	servoStatsTable *gui.Table
}

func NewRunScene(application *app.Application, serverConn *csTools.ServerConnObj) (*RunScene, error) {
	runScene := &RunScene{
		Node:        core.NewNode(),
		serverConn:  serverConn,
		savedPoses:  make(map[string][][]float32),
		currentPose: make([][]float32, 18),
	}

	//Create the array for each segment of the appendage
	for k, _ := range runScene.currentPose {
		runScene.currentPose[k] = make([]float32, 3)
	}

	// Label
	latencyWidth := 200
	latencyHeight := 50
	runScene.warnLatency = 10
	width, _ := application.GetSize()
	runScene.monitor.latencyText = "Latency: "
	runScene.monitor.latencyLabel = gui.NewLabel(runScene.monitor.latencyText)
	//-10 to give it the same buffer from the screen edge as the top
	runScene.monitor.latencyLabel.SetPosition(float32(width-latencyWidth-10), 10)
	runScene.monitor.latencyLabel.SetPaddings(2, 2, 2, 2)
	runScene.monitor.latencyLabel.SetFontSize(20)
	runScene.monitor.latencyLabel.SetSize(float32(latencyWidth), float32(latencyHeight))
	runScene.Add(runScene.monitor.latencyLabel)

	//TODO: add button on side for controller battery level
	//TODO: add button on side for bot battery level
	//TODO: when buttons clicked open a chart with battery levels over time

	//TODO: setup a hexapod with rectangles
	//width, height, depth from current camera angle
	//length, thickness, width
	runScene.createSpider()

	// Adds directional light
	l1 := light.NewDirectional(&math32.Color{0.4, 0.4, 0.4}, 1.0)
	l1.SetPosition(0, 0, 10)
	runScene.Add(l1)

	ambLight := light.NewAmbient(&math32.Color{1.0, 1.0, 1.0}, 0.5)
	runScene.Add(ambLight)

	formatAngle := func(cell gui.TableCell) string {
		if cell.Value == nil {
			return ""
		}
		angle := cell.Value.(float32)
		return strconv.FormatFloat(float64(angle), 'f', 2, 32)
	}

	formatVoltage := func(cell gui.TableCell) string {
		if cell.Value == nil {
			return ""
		}
		voltage := float32(cell.Value.(int)) / 1000
		return strconv.FormatFloat(float64(voltage), 'f', 3, 32)
	}

	//Add data output table
	tab, err := gui.NewTable(255, 500, []gui.TableColumn{
		{Id: "1", Header: "ID", Width: 20, Minwidth: 32, Align: gui.AlignCenter, Format: "%d", Resize: true, Sort: gui.TableSortNumber, Expand: 0},
		{Id: "2", Header: "Angle", Width: 95, Minwidth: 32, Align: gui.AlignCenter, FormatFunc: formatAngle, Resize: true, Expand: 0},
		{Id: "3", Header: "Voltage", Width: 75, Minwidth: 32, Align: gui.AlignCenter, FormatFunc: formatVoltage, Resize: true, Expand: 0},
		{Id: "4", Header: "Temp", Width: 65, Minwidth: 32, Align: gui.AlignCenter, Format: "%d°", Expand: 0},
	})
	if err != nil {
		return nil, err
	}

	tab.SetBorders(1, 1, 1, 1)
	tab.SetPosition(0, 0)
	tab.SetMargins(10, 10, 10, 10)
	tab.ShowStatus(true)
	//	tab.SetHeight(600)

	rows := make([]map[string]interface{}, 0, 18)
	for i := uint8(1); i <= 18; i++ {
		row := make(map[string]interface{})
		row["1"] = i
		row["2"] = float32(0)
		row["3"] = 0
		row["4"] = 0
		rows = append(rows, row)
	}
	tab.SetRows(rows)
	runScene.Add(tab)

	runScene.monitor.servoStatsTable = tab
	runScene.monitor.stopMonitor = make(chan bool)

	texNorm, err := texture.NewTexture2DFromImage("images/blue_normal.png")
	if err != nil {
		return nil, err
	}
	texPressed, err := texture.NewTexture2DFromImage("images/blue_pressed.png")
	if err != nil {
		return nil, err
	}

	savePoseButton, err := gui.NewImageButtonFromTexture(texNorm)
	if err != nil {
		return nil, err
	}
	savePoseButton.SetTexture(gui.ButtonPressed, texPressed)

	savePoseButton.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		runScene.savePose()
	})
	savePoseButton.SetPosition(float32(width-275), 50)
	savePoseButton.SetText("Save Pose")
	savePoseButton.SetFontSize(20)

	runScene.Add(savePoseButton)

	//menu on the side
	//display a hexapod in the center
	//manipulate the rectangles of the hexapod on the screen to reflect the current state of the hexapod
	//display estimated runtime remaining
	//display power draw
	return runScene, nil
}

func (runScene *RunScene) Start() {
	go runScene.startMonitor()
}
func (runScene *RunScene) Stop() {
	runScene.monitor.stopMonitor <- true
}

func (runScene *RunScene) startMonitor() {
	ticker := time.NewTicker(1 * time.Second)
	configuration, err := runScene.botClient.GetServoConfiguration()
	if err != nil {
		fmt.Println("Failed to get servo configuration. startMonitor() failed: ", err)
		return
	}
	runScene.servoConfiguration = configuration.Configuration

	for {
		select {
		case <-ticker.C:
			latency, err := runScene.serverConn.Ping(runScene.peer)
			if err != nil {
				fmt.Println("Failed to ping:", err)
			} else {
				runScene.monitor.latencyLabel.SetText(runScene.monitor.latencyText + strconv.Itoa(latency))
				if latency > runScene.warnLatency {
					runScene.monitor.latencyLabel.SetBgColor(math32.NewColor("red"))
				} else {
					runScene.monitor.latencyLabel.SetBgColor(math32.NewColor("green"))
				}
			}

			stats, err := runScene.botClient.GetAllServoStats()
			if err != nil {
				fmt.Println("Failed to get servo stats:", err)
			} else {
				for _, v := range stats {
					runScene.monitor.servoStatsTable.SetCell(int(v.ID)-1, "2", v.Angle)
					runScene.monitor.servoStatsTable.SetCell(int(v.ID)-1, "3", v.Voltage)
					runScene.monitor.servoStatsTable.SetCell(int(v.ID)-1, "4", CToF(v.Temp))

					//Update spider model
					joint := runScene.spiderModel.Joints[v.ID]
					joint.SetAngle(v.Angle)

				}
				runScene.monitor.servoStatsTable.SetStatusText("last update: " + time.Now().String())
			}
		case <-runScene.monitor.stopMonitor:
			ticker.Stop()
			break
		}
	}
}

// Celsius to Fahrenheit
func CToF(celsius int) int {
	return celsius/5*9 + 32
}

type Joint struct {
	*core.Node
	axis    math32.Vector3
	offset  float32
	angle   float32
	inverse bool
}

type SpiderModel struct {
	//[Arm][Joints]
	//	Appendages [][]int
	Joints map[uint8]*Joint
}

func (joint *Joint) GetAngle() float32 {
	return joint.angle
}

func (joint *Joint) SetAngle(angle float32) {
	if joint == nil {
		fmt.Println("joint nil")
		return
	}
	//	fmt.Println("Quat:", joint.Quaternion())
	var quat math32.Quaternion
	if joint.inverse {
		quat.SetFromAxisAngle(&joint.axis, math32.DegToRad(-angle+joint.offset))
	} else {
		quat.SetFromAxisAngle(&joint.axis, math32.DegToRad(angle+joint.offset))
	}
	joint.SetQuaternionQuat(&quat)
	joint.angle = angle
	//Get current angle
	//Compare the difference
	//apply the difference to the joint

	//	math32.DegToRad(45)
}

func (runScene *RunScene) createSpider() {
	spiderModel := SpiderModel{Joints: make(map[uint8]*Joint)}
	//measurements in mm
	//Measurements are multiplied by 0.001 to go from mm to meters
	bodyLength := float32(256.0) * .001
	bodyThickness := float32(40) * .001
	bodyWidth := float32(230.0) * .001

	appendageThickness := float32(40) * .001
	appendageWidth := float32(40) * .001

	coxaLength := float32(83.0) * .001
	femurLength := float32(157.0) * .001
	tibiaLength := float32(233.0) * .001

	//Create body of spider
	spiderRootNode := core.NewNode()
	bodyMat := material.NewStandard(&math32.Color{0.5, 0, 0})
	coxaMat := material.NewStandard(&math32.Color{0, 0.5, 0})
	femurMat := material.NewStandard(&math32.Color{0, 0, 0.5})
	tibiaMat := material.NewStandard(&math32.Color{0.5, 0, 0.5})

	bodyGeom := geometry.NewSegmentedBox(bodyWidth, bodyLength, bodyThickness, 1, 1, 1)
	body := graphic.NewMesh(bodyGeom, bodyMat)
	body.SetPosition(0, 0, 0)
	spiderRootNode.Add(body)

	coxaGeom := geometry.NewSegmentedBox(appendageWidth, coxaLength, appendageThickness, 1, 1, 1)
	femurGeom := geometry.NewSegmentedBox(appendageWidth, femurLength, appendageThickness, 1, 1, 1)
	tibiaGeom := geometry.NewSegmentedBox(appendageWidth, tibiaLength, appendageThickness, 1, 1, 1)

	// Leg 1
	coxaJoint1 := core.NewNode()
	coxa1 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa1.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa1.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint1.Add(coxa1)
	//Move joint to the side of body
	coxaJoint1.TranslateX(bodyWidth * .5)
	coxaJoint1.TranslateY(bodyLength * .5)
	body.Add(coxaJoint1)

	femurJoint1 := core.NewNode()
	femur1 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint1.Add(femur1)

	coxaJoint1.Add(femurJoint1)
	femur1.TranslateX(femurLength * 0.5)
	femur1.RotateZ(math32.DegToRad(90))
	femurJoint1.TranslateX(coxaLength)

	tibiaJoint1 := core.NewNode()
	tibia1 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint1.Add(tibia1)
	femurJoint1.Add(tibiaJoint1)
	tibia1.TranslateX(tibiaLength * 0.5)
	tibia1.RotateZ(math32.DegToRad(90))
	tibiaJoint1.TranslateX(femurLength)

	// Leg 2
	coxaJoint2 := core.NewNode()
	coxa2 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa2.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa2.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint2.Add(coxa2)
	//Move joint to the side of body
	coxaJoint2.TranslateX(bodyWidth * .5)
	body.Add(coxaJoint2)

	femurJoint2 := core.NewNode()
	femur2 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint2.Add(femur2)

	coxaJoint2.Add(femurJoint2)
	femur2.TranslateX(femurLength * 0.5)
	femur2.RotateZ(math32.DegToRad(90))
	femurJoint2.TranslateX(coxaLength)

	tibiaJoint2 := core.NewNode()
	tibia2 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint2.Add(tibia2)
	femurJoint2.Add(tibiaJoint2)
	tibia2.TranslateX(tibiaLength * 0.5)
	tibia2.RotateZ(math32.DegToRad(90))
	tibiaJoint2.TranslateX(femurLength)

	// Leg 3
	coxaJoint3 := core.NewNode()
	coxa3 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa3.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa3.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint3.Add(coxa3)
	//Move joint to the side of body
	coxaJoint3.TranslateX(bodyWidth * .5)
	coxaJoint3.TranslateY(-bodyLength * .5)
	body.Add(coxaJoint3)

	femurJoint3 := core.NewNode()
	femur3 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint3.Add(femur3)

	coxaJoint3.Add(femurJoint3)
	femur3.TranslateX(femurLength * 0.5)
	femur3.RotateZ(math32.DegToRad(90))
	femurJoint3.TranslateX(coxaLength)

	tibiaJoint3 := core.NewNode()
	tibia3 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint3.Add(tibia3)
	femurJoint3.Add(tibiaJoint3)
	tibia3.TranslateX(tibiaLength * 0.5)
	tibia3.RotateZ(math32.DegToRad(90))
	tibiaJoint3.TranslateX(femurLength)

	// Leg 4
	coxaJoint4 := core.NewNode()
	coxa4 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa4.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa4.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint4.Add(coxa4)
	//Move joint to the side of body
	coxaJoint4.TranslateX(-bodyWidth * .5)
	coxaJoint4.TranslateY(-bodyLength * .5)
	body.Add(coxaJoint4)

	femurJoint4 := core.NewNode()
	femur4 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint4.Add(femur4)

	coxaJoint4.Add(femurJoint4)
	femur4.TranslateX(femurLength * 0.5)
	femur4.RotateZ(math32.DegToRad(90))
	femurJoint4.TranslateX(coxaLength)

	tibiaJoint4 := core.NewNode()
	tibia4 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint4.Add(tibia4)
	femurJoint4.Add(tibiaJoint4)
	tibia4.TranslateX(tibiaLength * 0.5)
	tibia4.RotateZ(math32.DegToRad(90))
	tibiaJoint4.TranslateX(femurLength)
	coxaJoint4.RotateY(math32.DegToRad(180))
	coxaJoint4.RotateX(math32.DegToRad(180))

	// Leg 5
	coxaJoint5 := core.NewNode()
	coxa5 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa5.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa5.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint5.Add(coxa5)
	//Move joint to the side of body
	coxaJoint5.TranslateX(-bodyWidth * .5)
	body.Add(coxaJoint5)

	femurJoint5 := core.NewNode()
	femur5 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint5.Add(femur5)

	coxaJoint5.Add(femurJoint5)
	femur5.TranslateX(femurLength * 0.5)
	femur5.RotateZ(math32.DegToRad(90))
	femurJoint5.TranslateX(coxaLength)

	tibiaJoint5 := core.NewNode()
	tibia5 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint5.Add(tibia5)
	femurJoint5.Add(tibiaJoint5)
	tibia5.TranslateX(tibiaLength * 0.5)
	tibia5.RotateZ(math32.DegToRad(90))
	tibiaJoint5.TranslateX(femurLength)
	coxaJoint5.RotateY(math32.DegToRad(180))
	coxaJoint5.RotateX(math32.DegToRad(180))

	// Leg 6
	coxaJoint6 := core.NewNode()
	coxa6 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa6.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa6.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint6.Add(coxa6)
	//Move joint to the side of body
	coxaJoint6.TranslateX(-bodyWidth * .5)
	coxaJoint6.TranslateY(bodyLength * .5)
	body.Add(coxaJoint6)

	femurJoint6 := core.NewNode()
	femur6 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint6.Add(femur6)

	coxaJoint6.Add(femurJoint6)
	femur6.TranslateX(femurLength * 0.5)
	femur6.RotateZ(math32.DegToRad(90))
	femurJoint6.TranslateX(coxaLength)

	tibiaJoint6 := core.NewNode()
	tibia6 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint6.Add(tibia6)
	femurJoint6.Add(tibiaJoint6)
	tibia6.TranslateX(tibiaLength * 0.5)
	tibia6.RotateZ(math32.DegToRad(90))
	tibiaJoint6.TranslateX(femurLength)
	coxaJoint6.RotateY(math32.DegToRad(180))
	coxaJoint6.RotateX(math32.DegToRad(180))

	spiderModel.Joints[1] = &Joint{Node: coxaJoint1, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.Joints[2] = &Joint{Node: femurJoint1, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[3] = &Joint{Node: tibiaJoint1, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[4] = &Joint{Node: coxaJoint2, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.Joints[5] = &Joint{Node: femurJoint2, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[6] = &Joint{Node: tibiaJoint2, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[7] = &Joint{Node: coxaJoint3, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.Joints[8] = &Joint{Node: femurJoint3, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[9] = &Joint{Node: tibiaJoint3, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[10] = &Joint{Node: coxaJoint4, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}
	spiderModel.Joints[11] = &Joint{Node: femurJoint4, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[12] = &Joint{Node: tibiaJoint4, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[13] = &Joint{Node: coxaJoint5, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}
	spiderModel.Joints[14] = &Joint{Node: femurJoint5, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[15] = &Joint{Node: tibiaJoint5, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.Joints[16] = &Joint{Node: coxaJoint6, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}

	spiderModel.Joints[17] = &Joint{Node: femurJoint6, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.Joints[18] = &Joint{Node: tibiaJoint6, axis: math32.Vector3{0, 1, 0}, offset: -90}

	runScene.spiderModel = spiderModel

	//TODO: Remove this, its just a temporary positioning
	//////////////////////////////
	/*
		coxaJoint1.RotateZ(math32.DegToRad(45))
		femurJoint1.RotateY(math32.DegToRad(90))
		tibiaJoint1.RotateY(math32.DegToRad(45))

		coxaJoint2.RotateZ(math32.DegToRad(45))
		femurJoint2.RotateY(math32.DegToRad(90))
		tibiaJoint2.RotateY(math32.DegToRad(45))

		coxaJoint3.RotateZ(math32.DegToRad(-45))
		femurJoint3.RotateY(math32.DegToRad(90))
		tibiaJoint3.RotateY(math32.DegToRad(45))

		coxaJoint4.RotateZ(math32.DegToRad(45))
		femurJoint4.RotateY(math32.DegToRad(90))
		tibiaJoint4.RotateY(math32.DegToRad(45))

		coxaJoint5.RotateZ(math32.DegToRad(-45))
		femurJoint5.RotateY(math32.DegToRad(90))
		tibiaJoint5.RotateY(math32.DegToRad(45))

		coxaJoint6.RotateZ(math32.DegToRad(-45))
		femurJoint6.RotateY(math32.DegToRad(90))
		tibiaJoint6.RotateY(math32.DegToRad(45))
	*/
	//////////////////////////////

	runScene.Add(spiderRootNode)
}

func (runScene *RunScene) savePose() {
	newPose := make([][]float32, len(runScene.servoConfiguration))
	for k, v := range runScene.servoConfiguration {
		newPose[k] = make([]float32, len(v))
		for k2, v2 := range v {
			if runScene.spiderModel.Joints[v2] == nil {
				fmt.Println("Joint not set: ", v2)
				continue
			}
			newPose[k][k2] = runScene.spiderModel.Joints[v2].GetAngle()
		}
	}

	poseName := time.Now().String()
	fmt.Println("Saving pose: ", poseName)
	runScene.savedPoses[poseName] = newPose
	fmt.Println(runScene.savedPoses[poseName])
}
