package main

import (
	"gitlab.com/g3n/engine/core"
)

type Scene interface {
	core.INode
	Start()
	Stop()
}
